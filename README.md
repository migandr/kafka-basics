# spark-basics

This repository contains some basic structures of a Spark application


## To start Kafka:

If you want to modify any configuration parameter you should go to the files described at config.
The most common parameter to modify is DataDir, moving it from a tmp folder to something more
Easy to work with.

1- Open a terminal tab and go to de Kafka folder container:

	`zookeeper-server-start.sh config/zookeeper.properties`

2- Open another terminal tab on the same folder:

	`kafka-server-start.sh config/server.properties`

## To start a project from scratch

1- Create a maven project and fill the fields.

	GroupId 	->	com.gitlab.migandr
	ArtifactId 	->	kafka-tutorial

2- Add:
	- kafka dependencies
	- login dependencies

Look for kafka maven on google and select **kafka-clients** and copy the one of the technology that you are using for.

Then look on maven for `slf4j simple`once you hace copied the dependency you have to remove the scope test tag

3-Create new package
	com.gitlab.migandr.kafka

Inside of it create a new package with the name, i.e. tutorial1

Then create a JavaClass on int

4-To test the code, open a new console an add the following command:

	`kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 --topic <topic_name> --group <group_name>`
